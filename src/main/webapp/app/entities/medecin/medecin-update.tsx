import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm, ValidatedBlobField } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { ISecretaire } from 'app/shared/model/secretaire.model';
import { getEntities as getSecretaires } from 'app/entities/secretaire/secretaire.reducer';
import { IMedecin } from 'app/shared/model/medecin.model';
import { getEntity, updateEntity, createEntity, reset } from './medecin.reducer';

export const MedecinUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const users = useAppSelector(state => state.userManagement.users);
  const secretaires = useAppSelector(state => state.secretaire.entities);
  const medecinEntity = useAppSelector(state => state.medecin.entity);
  const loading = useAppSelector(state => state.medecin.loading);
  const updating = useAppSelector(state => state.medecin.updating);
  const updateSuccess = useAppSelector(state => state.medecin.updateSuccess);
  const handleClose = () => {
    props.history.push('/medecin');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getUsers({}));
    dispatch(getSecretaires({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...medecinEntity,
      ...values,
      user: users.find(it => it.id.toString() === values.user.toString()),
      secretaire: secretaires.find(it => it.id.toString() === values.secretaire.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...medecinEntity,
          user: medecinEntity?.user?.id,
          secretaire: medecinEntity?.secretaire?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.medecin.home.createOrEditLabel" data-cy="MedecinCreateUpdateHeading">
            Create or edit a Medecin
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="medecin-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Code" id="medecin-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Nom" id="medecin-nom" name="nom" data-cy="nom" type="text" />
              <ValidatedField label="Num Emp" id="medecin-numEmp" name="numEmp" data-cy="numEmp" type="text" />
              <ValidatedField label="Prenom" id="medecin-prenom" name="prenom" data-cy="prenom" type="text" />
              <ValidatedField label="Expert Level" id="medecin-expertLevel" name="expertLevel" data-cy="expertLevel" type="text" />
              <ValidatedBlobField label="Photo" id="medecin-photo" name="photo" data-cy="photo" isImage accept="image/*" />
              <ValidatedField label="Type" id="medecin-type" name="type" data-cy="type" type="text" />
              <ValidatedField label="Nbr Patients" id="medecin-nbrPatients" name="nbrPatients" data-cy="nbrPatients" type="text" />
              <ValidatedField label="Rating" id="medecin-rating" name="rating" data-cy="rating" type="text" />
              <ValidatedField label="Description" id="medecin-description" name="description" data-cy="description" type="textarea" />
              <ValidatedField id="medecin-user" name="user" data-cy="user" label="User" type="select">
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="medecin-secretaire" name="secretaire" data-cy="secretaire" label="Secretaire" type="select">
                <option value="" key="0" />
                {secretaires
                  ? secretaires.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/medecin" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default MedecinUpdate;
