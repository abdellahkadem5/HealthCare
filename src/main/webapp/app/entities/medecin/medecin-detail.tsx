import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './medecin.reducer';

export const MedecinDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const medecinEntity = useAppSelector(state => state.medecin.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="medecinDetailsHeading">Medecin</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{medecinEntity.id}</dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{medecinEntity.code}</dd>
          <dt>
            <span id="nom">Nom</span>
          </dt>
          <dd>{medecinEntity.nom}</dd>
          <dt>
            <span id="numEmp">Num Emp</span>
          </dt>
          <dd>{medecinEntity.numEmp}</dd>
          <dt>
            <span id="prenom">Prenom</span>
          </dt>
          <dd>{medecinEntity.prenom}</dd>
          <dt>
            <span id="expertLevel">Expert Level</span>
          </dt>
          <dd>{medecinEntity.expertLevel}</dd>
          <dt>
            <span id="photo">Photo</span>
          </dt>
          <dd>
            {medecinEntity.photo ? (
              <div>
                {medecinEntity.photoContentType ? (
                  <a onClick={openFile(medecinEntity.photoContentType, medecinEntity.photo)}>
                    <img src={`data:${medecinEntity.photoContentType};base64,${medecinEntity.photo}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {medecinEntity.photoContentType}, {byteSize(medecinEntity.photo)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>
            <span id="type">Type</span>
          </dt>
          <dd>{medecinEntity.type}</dd>
          <dt>
            <span id="nbrPatients">Nbr Patients</span>
          </dt>
          <dd>{medecinEntity.nbrPatients}</dd>
          <dt>
            <span id="rating">Rating</span>
          </dt>
          <dd>{medecinEntity.rating}</dd>
          <dt>
            <span id="description">Description</span>
          </dt>
          <dd>{medecinEntity.description}</dd>
          <dt>User</dt>
          <dd>{medecinEntity.user ? medecinEntity.user.id : ''}</dd>
          <dt>Secretaire</dt>
          <dd>{medecinEntity.secretaire ? medecinEntity.secretaire.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/medecin" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/medecin/${medecinEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default MedecinDetail;
