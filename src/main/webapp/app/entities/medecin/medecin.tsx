import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { openFile, byteSize, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IMedecin } from 'app/shared/model/medecin.model';
import { getEntities } from './medecin.reducer';

export const Medecin = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const medecinList = useAppSelector(state => state.medecin.entities);
  const loading = useAppSelector(state => state.medecin.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="medecin-heading" data-cy="MedecinHeading">
        Medecins
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to="/medecin/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Medecin
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {medecinList && medecinList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Code</th>
                <th>Nom</th>
                <th>Num Emp</th>
                <th>Prenom</th>
                <th>Expert Level</th>
                <th>Photo</th>
                <th>Type</th>
                <th>Nbr Patients</th>
                <th>Rating</th>
                <th>Description</th>
                <th>User</th>
                <th>Secretaire</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {medecinList.map((medecin, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/medecin/${medecin.id}`} color="link" size="sm">
                      {medecin.id}
                    </Button>
                  </td>
                  <td>{medecin.code}</td>
                  <td>{medecin.nom}</td>
                  <td>{medecin.numEmp}</td>
                  <td>{medecin.prenom}</td>
                  <td>{medecin.expertLevel}</td>
                  <td>
                    {medecin.photo ? (
                      <div>
                        {medecin.photoContentType ? (
                          <a onClick={openFile(medecin.photoContentType, medecin.photo)}>
                            <img src={`data:${medecin.photoContentType};base64,${medecin.photo}`} style={{ maxHeight: '30px' }} />
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {medecin.photoContentType}, {byteSize(medecin.photo)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{medecin.type}</td>
                  <td>{medecin.nbrPatients}</td>
                  <td>{medecin.rating}</td>
                  <td>{medecin.description}</td>
                  <td>{medecin.user ? medecin.user.id : ''}</td>
                  <td>{medecin.secretaire ? <Link to={`/secretaire/${medecin.secretaire.id}`}>{medecin.secretaire.id}</Link> : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/medecin/${medecin.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/medecin/${medecin.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/medecin/${medecin.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Medecins found</div>
        )}
      </div>
    </div>
  );
};

export default Medecin;
