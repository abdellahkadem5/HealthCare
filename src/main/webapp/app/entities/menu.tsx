import React from 'react';

import MenuItem from 'app/shared/layout/menus/menu-item';

const EntitiesMenu = () => {
  return (
    <>
      {/* prettier-ignore */}
      <MenuItem icon="asterisk" to="/medecin">
        Medecin
      </MenuItem>
      <MenuItem icon="asterisk" to="/secretaire">
        Secretaire
      </MenuItem>
      <MenuItem icon="asterisk" to="/patient">
        Patient
      </MenuItem>
      <MenuItem icon="asterisk" to="/detection">
        Detection
      </MenuItem>
      <MenuItem icon="asterisk" to="/rendez-vous">
        Rendez Vous
      </MenuItem>
      <MenuItem icon="asterisk" to="/visite">
        Visite
      </MenuItem>
      <MenuItem icon="asterisk" to="/maladie">
        Maladie
      </MenuItem>
      <MenuItem icon="asterisk" to="/classification">
        Classification
      </MenuItem>
      <MenuItem icon="asterisk" to="/image">
        Image
      </MenuItem>
      <MenuItem icon="asterisk" to="/unclassified">
        Unclassified
      </MenuItem>
      <MenuItem icon="asterisk" to="/stade">
        Stade
      </MenuItem>
      {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
    </>
  );
};

export default EntitiesMenu as React.ComponentType<any>;
