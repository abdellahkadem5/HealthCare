import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Visite from './visite';
import VisiteDetail from './visite-detail';
import VisiteUpdate from './visite-update';
import VisiteDeleteDialog from './visite-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={VisiteUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={VisiteUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={VisiteDetail} />
      <ErrorBoundaryRoute path={match.url} component={Visite} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={VisiteDeleteDialog} />
  </>
);

export default Routes;
