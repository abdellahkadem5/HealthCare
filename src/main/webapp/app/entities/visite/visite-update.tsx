import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IRendezVous } from 'app/shared/model/rendez-vous.model';
import { getEntities as getRendezVous } from 'app/entities/rendez-vous/rendez-vous.reducer';
import { IDetection } from 'app/shared/model/detection.model';
import { getEntities as getDetections } from 'app/entities/detection/detection.reducer';
import { IVisite } from 'app/shared/model/visite.model';
import { getEntity, updateEntity, createEntity, reset } from './visite.reducer';

export const VisiteUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const rendezVous = useAppSelector(state => state.rendezVous.entities);
  const detections = useAppSelector(state => state.detection.entities);
  const visiteEntity = useAppSelector(state => state.visite.entity);
  const loading = useAppSelector(state => state.visite.loading);
  const updating = useAppSelector(state => state.visite.updating);
  const updateSuccess = useAppSelector(state => state.visite.updateSuccess);
  const handleClose = () => {
    props.history.push('/visite');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getRendezVous({}));
    dispatch(getDetections({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...visiteEntity,
      ...values,
      rendezVous: rendezVous.find(it => it.id.toString() === values.rendezVous.toString()),
      detection: detections.find(it => it.id.toString() === values.detection.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...visiteEntity,
          rendezVous: visiteEntity?.rendezVous?.id,
          detection: visiteEntity?.detection?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.visite.home.createOrEditLabel" data-cy="VisiteCreateUpdateHeading">
            Create or edit a Visite
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="visite-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Code" id="visite-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Date" id="visite-date" name="date" data-cy="date" type="date" />
              <ValidatedField id="visite-rendezVous" name="rendezVous" data-cy="rendezVous" label="Rendez Vous" type="select">
                <option value="" key="0" />
                {rendezVous
                  ? rendezVous.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="visite-detection" name="detection" data-cy="detection" label="Detection" type="select">
                <option value="" key="0" />
                {detections
                  ? detections.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/visite" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default VisiteUpdate;
