import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Detection from './detection';
import DetectionDetail from './detection-detail';
import DetectionUpdate from './detection-update';
import DetectionDeleteDialog from './detection-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DetectionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DetectionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DetectionDetail} />
      <ErrorBoundaryRoute path={match.url} component={Detection} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DetectionDeleteDialog} />
  </>
);

export default Routes;
