import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm, ValidatedBlobField } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IMaladie } from 'app/shared/model/maladie.model';
import { getEntities as getMaladies } from 'app/entities/maladie/maladie.reducer';
import { IPatient } from 'app/shared/model/patient.model';
import { getEntities as getPatients } from 'app/entities/patient/patient.reducer';
import { IVisite } from 'app/shared/model/visite.model';
import { getEntities as getVisites } from 'app/entities/visite/visite.reducer';
import { IDetection } from 'app/shared/model/detection.model';
import { getEntity, updateEntity, createEntity, reset } from './detection.reducer';

export const DetectionUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const maladies = useAppSelector(state => state.maladie.entities);
  const patients = useAppSelector(state => state.patient.entities);
  const visites = useAppSelector(state => state.visite.entities);
  const detectionEntity = useAppSelector(state => state.detection.entity);
  const loading = useAppSelector(state => state.detection.loading);
  const updating = useAppSelector(state => state.detection.updating);
  const updateSuccess = useAppSelector(state => state.detection.updateSuccess);
  const handleClose = () => {
    props.history.push('/detection');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getMaladies({}));
    dispatch(getPatients({}));
    dispatch(getVisites({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.date = convertDateTimeToServer(values.date);

    const entity = {
      ...detectionEntity,
      ...values,
      maladie: maladies.find(it => it.id.toString() === values.maladie.toString()),
      patient: patients.find(it => it.id.toString() === values.patient.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          date: displayDefaultDateTime(),
        }
      : {
          ...detectionEntity,
          date: convertDateTimeFromServer(detectionEntity.date),
          maladie: detectionEntity?.maladie?.id,
          patient: detectionEntity?.patient?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.detection.home.createOrEditLabel" data-cy="DetectionCreateUpdateHeading">
            Create or edit a Detection
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="detection-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedBlobField label="Photo" id="detection-photo" name="photo" data-cy="photo" isImage accept="image/*" />
              <ValidatedField label="Code" id="detection-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Validation" id="detection-validation" name="validation" data-cy="validation" check type="checkbox" />
              <ValidatedField label="Stade" id="detection-stade" name="stade" data-cy="stade" type="text" />
              <ValidatedField
                label="Date"
                id="detection-date"
                name="date"
                data-cy="date"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField label="Description" id="detection-description" name="description" data-cy="description" type="text" />
              <ValidatedField id="detection-maladie" name="maladie" data-cy="maladie" label="Maladie" type="select">
                <option value="" key="0" />
                {maladies
                  ? maladies.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="detection-patient" name="patient" data-cy="patient" label="Patient" type="select">
                <option value="" key="0" />
                {patients
                  ? patients.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/detection" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default DetectionUpdate;
