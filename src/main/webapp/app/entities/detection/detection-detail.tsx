import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './detection.reducer';

export const DetectionDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const detectionEntity = useAppSelector(state => state.detection.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="detectionDetailsHeading">Detection</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{detectionEntity.id}</dd>
          <dt>
            <span id="photo">Photo</span>
          </dt>
          <dd>
            {detectionEntity.photo ? (
              <div>
                {detectionEntity.photoContentType ? (
                  <a onClick={openFile(detectionEntity.photoContentType, detectionEntity.photo)}>
                    <img src={`data:${detectionEntity.photoContentType};base64,${detectionEntity.photo}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {detectionEntity.photoContentType}, {byteSize(detectionEntity.photo)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{detectionEntity.code}</dd>
          <dt>
            <span id="validation">Validation</span>
          </dt>
          <dd>{detectionEntity.validation ? 'true' : 'false'}</dd>
          <dt>
            <span id="stade">Stade</span>
          </dt>
          <dd>{detectionEntity.stade}</dd>
          <dt>
            <span id="date">Date</span>
          </dt>
          <dd>{detectionEntity.date ? <TextFormat value={detectionEntity.date} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="description">Description</span>
          </dt>
          <dd>{detectionEntity.description}</dd>
          <dt>Maladie</dt>
          <dd>{detectionEntity.maladie ? detectionEntity.maladie.id : ''}</dd>
          <dt>Patient</dt>
          <dd>{detectionEntity.patient ? detectionEntity.patient.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/detection" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/detection/${detectionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default DetectionDetail;
