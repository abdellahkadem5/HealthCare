import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './patient.reducer';

export const PatientDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const patientEntity = useAppSelector(state => state.patient.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="patientDetailsHeading">Patient</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{patientEntity.id}</dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{patientEntity.code}</dd>
          <dt>
            <span id="nom">Nom</span>
          </dt>
          <dd>{patientEntity.nom}</dd>
          <dt>
            <span id="prenom">Prenom</span>
          </dt>
          <dd>{patientEntity.prenom}</dd>
          <dt>
            <span id="dateNaissance">Date Naissance</span>
          </dt>
          <dd>
            {patientEntity.dateNaissance ? (
              <TextFormat value={patientEntity.dateNaissance} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="adresse">Adresse</span>
          </dt>
          <dd>{patientEntity.adresse}</dd>
          <dt>
            <span id="genre">Genre</span>
          </dt>
          <dd>{patientEntity.genre}</dd>
          <dt>
            <span id="telephone">Telephone</span>
          </dt>
          <dd>{patientEntity.telephone}</dd>
          <dt>
            <span id="poids">Poids</span>
          </dt>
          <dd>{patientEntity.poids}</dd>
          <dt>
            <span id="taille">Taille</span>
          </dt>
          <dd>{patientEntity.taille}</dd>
          <dt>
            <span id="photo">Photo</span>
          </dt>
          <dd>
            {patientEntity.photo ? (
              <div>
                {patientEntity.photoContentType ? (
                  <a onClick={openFile(patientEntity.photoContentType, patientEntity.photo)}>
                    <img src={`data:${patientEntity.photoContentType};base64,${patientEntity.photo}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {patientEntity.photoContentType}, {byteSize(patientEntity.photo)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>User</dt>
          <dd>{patientEntity.user ? patientEntity.user.id : ''}</dd>
          <dt>Secretaire</dt>
          <dd>{patientEntity.secretaire ? patientEntity.secretaire.id : ''}</dd>
          <dt>Stade</dt>
          <dd>{patientEntity.stade ? patientEntity.stade.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/patient" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/patient/${patientEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default PatientDetail;
