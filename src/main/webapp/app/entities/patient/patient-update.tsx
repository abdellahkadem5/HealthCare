import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm, ValidatedBlobField } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { ISecretaire } from 'app/shared/model/secretaire.model';
import { getEntities as getSecretaires } from 'app/entities/secretaire/secretaire.reducer';
import { IStade } from 'app/shared/model/stade.model';
import { getEntities as getStades } from 'app/entities/stade/stade.reducer';
import { IPatient } from 'app/shared/model/patient.model';
import { Genre } from 'app/shared/model/enumerations/genre.model';
import { getEntity, updateEntity, createEntity, reset } from './patient.reducer';

export const PatientUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const users = useAppSelector(state => state.userManagement.users);
  const secretaires = useAppSelector(state => state.secretaire.entities);
  const stades = useAppSelector(state => state.stade.entities);
  const patientEntity = useAppSelector(state => state.patient.entity);
  const loading = useAppSelector(state => state.patient.loading);
  const updating = useAppSelector(state => state.patient.updating);
  const updateSuccess = useAppSelector(state => state.patient.updateSuccess);
  const genreValues = Object.keys(Genre);
  const handleClose = () => {
    props.history.push('/patient');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getUsers({}));
    dispatch(getSecretaires({}));
    dispatch(getStades({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...patientEntity,
      ...values,
      user: users.find(it => it.id.toString() === values.user.toString()),
      secretaire: secretaires.find(it => it.id.toString() === values.secretaire.toString()),
      stade: stades.find(it => it.id.toString() === values.stade.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          genre: 'Homme',
          ...patientEntity,
          user: patientEntity?.user?.id,
          secretaire: patientEntity?.secretaire?.id,
          stade: patientEntity?.stade?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.patient.home.createOrEditLabel" data-cy="PatientCreateUpdateHeading">
            Create or edit a Patient
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="patient-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Code" id="patient-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Nom" id="patient-nom" name="nom" data-cy="nom" type="text" />
              <ValidatedField label="Prenom" id="patient-prenom" name="prenom" data-cy="prenom" type="text" />
              <ValidatedField label="Date Naissance" id="patient-dateNaissance" name="dateNaissance" data-cy="dateNaissance" type="date" />
              <ValidatedField label="Adresse" id="patient-adresse" name="adresse" data-cy="adresse" type="text" />
              <ValidatedField label="Genre" id="patient-genre" name="genre" data-cy="genre" type="select">
                {genreValues.map(genre => (
                  <option value={genre} key={genre}>
                    {genre}
                  </option>
                ))}
              </ValidatedField>
              <ValidatedField label="Telephone" id="patient-telephone" name="telephone" data-cy="telephone" type="text" />
              <ValidatedField label="Poids" id="patient-poids" name="poids" data-cy="poids" type="text" />
              <ValidatedField label="Taille" id="patient-taille" name="taille" data-cy="taille" type="text" />
              <ValidatedBlobField label="Photo" id="patient-photo" name="photo" data-cy="photo" isImage accept="image/*" />
              <ValidatedField id="patient-user" name="user" data-cy="user" label="User" type="select">
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="patient-secretaire" name="secretaire" data-cy="secretaire" label="Secretaire" type="select">
                <option value="" key="0" />
                {secretaires
                  ? secretaires.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="patient-stade" name="stade" data-cy="stade" label="Stade" type="select">
                <option value="" key="0" />
                {stades
                  ? stades.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/patient" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default PatientUpdate;
