import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { openFile, byteSize, Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IPatient } from 'app/shared/model/patient.model';
import { getEntities } from './patient.reducer';

export const Patient = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const patientList = useAppSelector(state => state.patient.entities);
  const loading = useAppSelector(state => state.patient.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="patient-heading" data-cy="PatientHeading">
        Patients
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to="/patient/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Patient
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {patientList && patientList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Code</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Date Naissance</th>
                <th>Adresse</th>
                <th>Genre</th>
                <th>Telephone</th>
                <th>Poids</th>
                <th>Taille</th>
                <th>Photo</th>
                <th>User</th>
                <th>Secretaire</th>
                <th>Stade</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {patientList.map((patient, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/patient/${patient.id}`} color="link" size="sm">
                      {patient.id}
                    </Button>
                  </td>
                  <td>{patient.code}</td>
                  <td>{patient.nom}</td>
                  <td>{patient.prenom}</td>
                  <td>
                    {patient.dateNaissance ? <TextFormat type="date" value={patient.dateNaissance} format={APP_LOCAL_DATE_FORMAT} /> : null}
                  </td>
                  <td>{patient.adresse}</td>
                  <td>{patient.genre}</td>
                  <td>{patient.telephone}</td>
                  <td>{patient.poids}</td>
                  <td>{patient.taille}</td>
                  <td>
                    {patient.photo ? (
                      <div>
                        {patient.photoContentType ? (
                          <a onClick={openFile(patient.photoContentType, patient.photo)}>
                            <img src={`data:${patient.photoContentType};base64,${patient.photo}`} style={{ maxHeight: '30px' }} />
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {patient.photoContentType}, {byteSize(patient.photo)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{patient.user ? patient.user.id : ''}</td>
                  <td>{patient.secretaire ? <Link to={`/secretaire/${patient.secretaire.id}`}>{patient.secretaire.id}</Link> : ''}</td>
                  <td>{patient.stade ? <Link to={`/stade/${patient.stade.id}`}>{patient.stade.id}</Link> : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/patient/${patient.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/patient/${patient.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/patient/${patient.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Patients found</div>
        )}
      </div>
    </div>
  );
};

export default Patient;
