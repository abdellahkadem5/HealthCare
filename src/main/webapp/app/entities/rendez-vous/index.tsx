import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import RendezVous from './rendez-vous';
import RendezVousDetail from './rendez-vous-detail';
import RendezVousUpdate from './rendez-vous-update';
import RendezVousDeleteDialog from './rendez-vous-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RendezVousUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={RendezVousUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={RendezVousDetail} />
      <ErrorBoundaryRoute path={match.url} component={RendezVous} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={RendezVousDeleteDialog} />
  </>
);

export default Routes;
