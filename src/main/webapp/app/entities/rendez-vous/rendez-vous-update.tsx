import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IPatient } from 'app/shared/model/patient.model';
import { getEntities as getPatients } from 'app/entities/patient/patient.reducer';
import { IMedecin } from 'app/shared/model/medecin.model';
import { getEntities as getMedecins } from 'app/entities/medecin/medecin.reducer';
import { IVisite } from 'app/shared/model/visite.model';
import { getEntities as getVisites } from 'app/entities/visite/visite.reducer';
import { IRendezVous } from 'app/shared/model/rendez-vous.model';
import { getEntity, updateEntity, createEntity, reset } from './rendez-vous.reducer';

export const RendezVousUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const patients = useAppSelector(state => state.patient.entities);
  const medecins = useAppSelector(state => state.medecin.entities);
  const visites = useAppSelector(state => state.visite.entities);
  const rendezVousEntity = useAppSelector(state => state.rendezVous.entity);
  const loading = useAppSelector(state => state.rendezVous.loading);
  const updating = useAppSelector(state => state.rendezVous.updating);
  const updateSuccess = useAppSelector(state => state.rendezVous.updateSuccess);
  const handleClose = () => {
    props.history.push('/rendez-vous');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getPatients({}));
    dispatch(getMedecins({}));
    dispatch(getVisites({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.date = convertDateTimeToServer(values.date);

    const entity = {
      ...rendezVousEntity,
      ...values,
      patient: patients.find(it => it.id.toString() === values.patient.toString()),
      medecin: medecins.find(it => it.id.toString() === values.medecin.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          date: displayDefaultDateTime(),
        }
      : {
          ...rendezVousEntity,
          date: convertDateTimeFromServer(rendezVousEntity.date),
          patient: rendezVousEntity?.patient?.id,
          medecin: rendezVousEntity?.medecin?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.rendezVous.home.createOrEditLabel" data-cy="RendezVousCreateUpdateHeading">
            Create or edit a RendezVous
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="rendez-vous-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField
                label="Date"
                id="rendez-vous-date"
                name="date"
                data-cy="date"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField label="Code" id="rendez-vous-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Status" id="rendez-vous-status" name="status" data-cy="status" type="text" />
              <ValidatedField id="rendez-vous-patient" name="patient" data-cy="patient" label="Patient" type="select">
                <option value="" key="0" />
                {patients
                  ? patients.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="rendez-vous-medecin" name="medecin" data-cy="medecin" label="Medecin" type="select">
                <option value="" key="0" />
                {medecins
                  ? medecins.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/rendez-vous" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default RendezVousUpdate;
