import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IRendezVous } from 'app/shared/model/rendez-vous.model';
import { getEntities } from './rendez-vous.reducer';

export const RendezVous = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const rendezVousList = useAppSelector(state => state.rendezVous.entities);
  const loading = useAppSelector(state => state.rendezVous.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="rendez-vous-heading" data-cy="RendezVousHeading">
        Rendez Vous
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to="/rendez-vous/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Rendez Vous
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {rendezVousList && rendezVousList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Code</th>
                <th>Status</th>
                <th>Patient</th>
                <th>Medecin</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {rendezVousList.map((rendezVous, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/rendez-vous/${rendezVous.id}`} color="link" size="sm">
                      {rendezVous.id}
                    </Button>
                  </td>
                  <td>{rendezVous.date ? <TextFormat type="date" value={rendezVous.date} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>{rendezVous.code}</td>
                  <td>{rendezVous.status}</td>
                  <td>{rendezVous.patient ? <Link to={`/patient/${rendezVous.patient.id}`}>{rendezVous.patient.id}</Link> : ''}</td>
                  <td>{rendezVous.medecin ? <Link to={`/medecin/${rendezVous.medecin.id}`}>{rendezVous.medecin.id}</Link> : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/rendez-vous/${rendezVous.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/rendez-vous/${rendezVous.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/rendez-vous/${rendezVous.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Rendez Vous found</div>
        )}
      </div>
    </div>
  );
};

export default RendezVous;
