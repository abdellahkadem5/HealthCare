import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './rendez-vous.reducer';

export const RendezVousDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const rendezVousEntity = useAppSelector(state => state.rendezVous.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="rendezVousDetailsHeading">RendezVous</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{rendezVousEntity.id}</dd>
          <dt>
            <span id="date">Date</span>
          </dt>
          <dd>{rendezVousEntity.date ? <TextFormat value={rendezVousEntity.date} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{rendezVousEntity.code}</dd>
          <dt>
            <span id="status">Status</span>
          </dt>
          <dd>{rendezVousEntity.status}</dd>
          <dt>Patient</dt>
          <dd>{rendezVousEntity.patient ? rendezVousEntity.patient.id : ''}</dd>
          <dt>Medecin</dt>
          <dd>{rendezVousEntity.medecin ? rendezVousEntity.medecin.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/rendez-vous" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/rendez-vous/${rendezVousEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default RendezVousDetail;
