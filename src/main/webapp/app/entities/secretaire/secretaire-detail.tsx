import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './secretaire.reducer';

export const SecretaireDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const secretaireEntity = useAppSelector(state => state.secretaire.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="secretaireDetailsHeading">Secretaire</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{secretaireEntity.id}</dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{secretaireEntity.code}</dd>
          <dt>
            <span id="nom">Nom</span>
          </dt>
          <dd>{secretaireEntity.nom}</dd>
          <dt>
            <span id="numEmp">Num Emp</span>
          </dt>
          <dd>{secretaireEntity.numEmp}</dd>
          <dt>
            <span id="prenom">Prenom</span>
          </dt>
          <dd>{secretaireEntity.prenom}</dd>
          <dt>
            <span id="admin">Admin</span>
          </dt>
          <dd>{secretaireEntity.admin ? 'true' : 'false'}</dd>
          <dt>
            <span id="photo">Photo</span>
          </dt>
          <dd>
            {secretaireEntity.photo ? (
              <div>
                {secretaireEntity.photoContentType ? (
                  <a onClick={openFile(secretaireEntity.photoContentType, secretaireEntity.photo)}>
                    <img src={`data:${secretaireEntity.photoContentType};base64,${secretaireEntity.photo}`} style={{ maxHeight: '30px' }} />
                  </a>
                ) : null}
                <span>
                  {secretaireEntity.photoContentType}, {byteSize(secretaireEntity.photo)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>User</dt>
          <dd>{secretaireEntity.user ? secretaireEntity.user.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/secretaire" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/secretaire/${secretaireEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default SecretaireDetail;
