import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Secretaire from './secretaire';
import SecretaireDetail from './secretaire-detail';
import SecretaireUpdate from './secretaire-update';
import SecretaireDeleteDialog from './secretaire-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SecretaireUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SecretaireUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SecretaireDetail} />
      <ErrorBoundaryRoute path={match.url} component={Secretaire} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={SecretaireDeleteDialog} />
  </>
);

export default Routes;
