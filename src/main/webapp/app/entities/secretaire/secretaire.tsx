import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { openFile, byteSize, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { ISecretaire } from 'app/shared/model/secretaire.model';
import { getEntities } from './secretaire.reducer';

export const Secretaire = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const secretaireList = useAppSelector(state => state.secretaire.entities);
  const loading = useAppSelector(state => state.secretaire.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="secretaire-heading" data-cy="SecretaireHeading">
        Secretaires
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to="/secretaire/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Secretaire
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {secretaireList && secretaireList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Code</th>
                <th>Nom</th>
                <th>Num Emp</th>
                <th>Prenom</th>
                <th>Admin</th>
                <th>Photo</th>
                <th>User</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {secretaireList.map((secretaire, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/secretaire/${secretaire.id}`} color="link" size="sm">
                      {secretaire.id}
                    </Button>
                  </td>
                  <td>{secretaire.code}</td>
                  <td>{secretaire.nom}</td>
                  <td>{secretaire.numEmp}</td>
                  <td>{secretaire.prenom}</td>
                  <td>{secretaire.admin ? 'true' : 'false'}</td>
                  <td>
                    {secretaire.photo ? (
                      <div>
                        {secretaire.photoContentType ? (
                          <a onClick={openFile(secretaire.photoContentType, secretaire.photo)}>
                            <img src={`data:${secretaire.photoContentType};base64,${secretaire.photo}`} style={{ maxHeight: '30px' }} />
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {secretaire.photoContentType}, {byteSize(secretaire.photo)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{secretaire.user ? secretaire.user.id : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/secretaire/${secretaire.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/secretaire/${secretaire.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`/secretaire/${secretaire.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Secretaires found</div>
        )}
      </div>
    </div>
  );
};

export default Secretaire;
