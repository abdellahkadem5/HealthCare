import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm, ValidatedBlobField } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { ISecretaire } from 'app/shared/model/secretaire.model';
import { getEntity, updateEntity, createEntity, reset } from './secretaire.reducer';

export const SecretaireUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const users = useAppSelector(state => state.userManagement.users);
  const secretaireEntity = useAppSelector(state => state.secretaire.entity);
  const loading = useAppSelector(state => state.secretaire.loading);
  const updating = useAppSelector(state => state.secretaire.updating);
  const updateSuccess = useAppSelector(state => state.secretaire.updateSuccess);
  const handleClose = () => {
    props.history.push('/secretaire');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getUsers({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...secretaireEntity,
      ...values,
      user: users.find(it => it.id.toString() === values.user.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...secretaireEntity,
          user: secretaireEntity?.user?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.secretaire.home.createOrEditLabel" data-cy="SecretaireCreateUpdateHeading">
            Create or edit a Secretaire
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="secretaire-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Code" id="secretaire-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Nom" id="secretaire-nom" name="nom" data-cy="nom" type="text" />
              <ValidatedField label="Num Emp" id="secretaire-numEmp" name="numEmp" data-cy="numEmp" type="text" />
              <ValidatedField label="Prenom" id="secretaire-prenom" name="prenom" data-cy="prenom" type="text" />
              <ValidatedField label="Admin" id="secretaire-admin" name="admin" data-cy="admin" check type="checkbox" />
              <ValidatedBlobField label="Photo" id="secretaire-photo" name="photo" data-cy="photo" isImage accept="image/*" />
              <ValidatedField id="secretaire-user" name="user" data-cy="user" label="User" type="select">
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/secretaire" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default SecretaireUpdate;
