import medecin from 'app/entities/medecin/medecin.reducer';
import secretaire from 'app/entities/secretaire/secretaire.reducer';
import patient from 'app/entities/patient/patient.reducer';
import detection from 'app/entities/detection/detection.reducer';
import rendezVous from 'app/entities/rendez-vous/rendez-vous.reducer';
import visite from 'app/entities/visite/visite.reducer';
import maladie from 'app/entities/maladie/maladie.reducer';
import classification from 'app/entities/classification/classification.reducer';
import image from 'app/entities/image/image.reducer';
import unclassified from 'app/entities/unclassified/unclassified.reducer';
import stade from 'app/entities/stade/stade.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  medecin,
  secretaire,
  patient,
  detection,
  rendezVous,
  visite,
  maladie,
  classification,
  image,
  unclassified,
  stade,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
