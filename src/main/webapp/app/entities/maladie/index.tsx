import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Maladie from './maladie';
import MaladieDetail from './maladie-detail';
import MaladieUpdate from './maladie-update';
import MaladieDeleteDialog from './maladie-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={MaladieUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={MaladieUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={MaladieDetail} />
      <ErrorBoundaryRoute path={match.url} component={Maladie} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={MaladieDeleteDialog} />
  </>
);

export default Routes;
