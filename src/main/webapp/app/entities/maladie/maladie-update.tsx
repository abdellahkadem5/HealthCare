import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IDetection } from 'app/shared/model/detection.model';
import { getEntities as getDetections } from 'app/entities/detection/detection.reducer';
import { IMaladie } from 'app/shared/model/maladie.model';
import { getEntity, updateEntity, createEntity, reset } from './maladie.reducer';

export const MaladieUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const detections = useAppSelector(state => state.detection.entities);
  const maladieEntity = useAppSelector(state => state.maladie.entity);
  const loading = useAppSelector(state => state.maladie.loading);
  const updating = useAppSelector(state => state.maladie.updating);
  const updateSuccess = useAppSelector(state => state.maladie.updateSuccess);
  const handleClose = () => {
    props.history.push('/maladie');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getDetections({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...maladieEntity,
      ...values,
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...maladieEntity,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.maladie.home.createOrEditLabel" data-cy="MaladieCreateUpdateHeading">
            Create or edit a Maladie
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="maladie-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Code" id="maladie-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Date" id="maladie-date" name="date" data-cy="date" type="date" />
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/maladie" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default MaladieUpdate;
