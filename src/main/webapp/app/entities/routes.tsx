import React from 'react';
import { Switch } from 'react-router-dom';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Medecin from './medecin';
import Secretaire from './secretaire';
import Patient from './patient';
import Detection from './detection';
import RendezVous from './rendez-vous';
import Visite from './visite';
import Maladie from './maladie';
import Classification from './classification';
import Image from './image';
import Unclassified from './unclassified';
import Stade from './stade';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default ({ match }) => {
  return (
    <div>
      <Switch>
        {/* prettier-ignore */}
        <ErrorBoundaryRoute path={`${match.url}medecin`} component={Medecin} />
        <ErrorBoundaryRoute path={`${match.url}secretaire`} component={Secretaire} />
        <ErrorBoundaryRoute path={`${match.url}patient`} component={Patient} />
        <ErrorBoundaryRoute path={`${match.url}detection`} component={Detection} />
        <ErrorBoundaryRoute path={`${match.url}rendez-vous`} component={RendezVous} />
        <ErrorBoundaryRoute path={`${match.url}visite`} component={Visite} />
        <ErrorBoundaryRoute path={`${match.url}maladie`} component={Maladie} />
        <ErrorBoundaryRoute path={`${match.url}classification`} component={Classification} />
        <ErrorBoundaryRoute path={`${match.url}image`} component={Image} />
        <ErrorBoundaryRoute path={`${match.url}unclassified`} component={Unclassified} />
        <ErrorBoundaryRoute path={`${match.url}stade`} component={Stade} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </Switch>
    </div>
  );
};
