import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IMaladie } from 'app/shared/model/maladie.model';
import { getEntities as getMaladies } from 'app/entities/maladie/maladie.reducer';
import { IStade } from 'app/shared/model/stade.model';
import { getEntity, updateEntity, createEntity, reset } from './stade.reducer';

export const StadeUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const maladies = useAppSelector(state => state.maladie.entities);
  const stadeEntity = useAppSelector(state => state.stade.entity);
  const loading = useAppSelector(state => state.stade.loading);
  const updating = useAppSelector(state => state.stade.updating);
  const updateSuccess = useAppSelector(state => state.stade.updateSuccess);
  const handleClose = () => {
    props.history.push('/stade');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getMaladies({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...stadeEntity,
      ...values,
      maladie: maladies.find(it => it.id.toString() === values.maladie.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...stadeEntity,
          maladie: stadeEntity?.maladie?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.stade.home.createOrEditLabel" data-cy="StadeCreateUpdateHeading">
            Create or edit a Stade
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="stade-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Code" id="stade-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Level" id="stade-level" name="level" data-cy="level" type="text" />
              <ValidatedField label="Description" id="stade-description" name="description" data-cy="description" type="text" />
              <ValidatedField id="stade-maladie" name="maladie" data-cy="maladie" label="Maladie" type="select">
                <option value="" key="0" />
                {maladies
                  ? maladies.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/stade" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default StadeUpdate;
