import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './stade.reducer';

export const StadeDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const stadeEntity = useAppSelector(state => state.stade.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="stadeDetailsHeading">Stade</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{stadeEntity.id}</dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{stadeEntity.code}</dd>
          <dt>
            <span id="level">Level</span>
          </dt>
          <dd>{stadeEntity.level}</dd>
          <dt>
            <span id="description">Description</span>
          </dt>
          <dd>{stadeEntity.description}</dd>
          <dt>Maladie</dt>
          <dd>{stadeEntity.maladie ? stadeEntity.maladie.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/stade" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/stade/${stadeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default StadeDetail;
