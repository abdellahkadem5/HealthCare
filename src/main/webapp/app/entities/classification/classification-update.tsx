import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IMedecin } from 'app/shared/model/medecin.model';
import { getEntities as getMedecins } from 'app/entities/medecin/medecin.reducer';
import { IStade } from 'app/shared/model/stade.model';
import { getEntities as getStades } from 'app/entities/stade/stade.reducer';
import { IUnclassified } from 'app/shared/model/unclassified.model';
import { getEntities as getUnclassifieds } from 'app/entities/unclassified/unclassified.reducer';
import { IClassification } from 'app/shared/model/classification.model';
import { getEntity, updateEntity, createEntity, reset } from './classification.reducer';

export const ClassificationUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const medecins = useAppSelector(state => state.medecin.entities);
  const stades = useAppSelector(state => state.stade.entities);
  const unclassifieds = useAppSelector(state => state.unclassified.entities);
  const classificationEntity = useAppSelector(state => state.classification.entity);
  const loading = useAppSelector(state => state.classification.loading);
  const updating = useAppSelector(state => state.classification.updating);
  const updateSuccess = useAppSelector(state => state.classification.updateSuccess);
  const handleClose = () => {
    props.history.push('/classification');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getMedecins({}));
    dispatch(getStades({}));
    dispatch(getUnclassifieds({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...classificationEntity,
      ...values,
      medecin: medecins.find(it => it.id.toString() === values.medecin.toString()),
      stade: stades.find(it => it.id.toString() === values.stade.toString()),
      unclassified: unclassifieds.find(it => it.id.toString() === values.unclassified.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...classificationEntity,
          medecin: classificationEntity?.medecin?.id,
          stade: classificationEntity?.stade?.id,
          unclassified: classificationEntity?.unclassified?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.classification.home.createOrEditLabel" data-cy="ClassificationCreateUpdateHeading">
            Create or edit a Classification
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField name="id" required readOnly id="classification-id" label="ID" validate={{ required: true }} />
              ) : null}
              <ValidatedField label="Code" id="classification-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedField label="Score" id="classification-score" name="score" data-cy="score" check type="checkbox" />
              <ValidatedField id="classification-medecin" name="medecin" data-cy="medecin" label="Medecin" type="select">
                <option value="" key="0" />
                {medecins
                  ? medecins.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="classification-stade" name="stade" data-cy="stade" label="Stade" type="select">
                <option value="" key="0" />
                {stades
                  ? stades.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="classification-unclassified"
                name="unclassified"
                data-cy="unclassified"
                label="Unclassified"
                type="select"
              >
                <option value="" key="0" />
                {unclassifieds
                  ? unclassifieds.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/classification" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default ClassificationUpdate;
