import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './classification.reducer';

export const ClassificationDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const classificationEntity = useAppSelector(state => state.classification.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="classificationDetailsHeading">Classification</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{classificationEntity.id}</dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{classificationEntity.code}</dd>
          <dt>
            <span id="score">Score</span>
          </dt>
          <dd>{classificationEntity.score ? 'true' : 'false'}</dd>
          <dt>Medecin</dt>
          <dd>{classificationEntity.medecin ? classificationEntity.medecin.id : ''}</dd>
          <dt>Stade</dt>
          <dd>{classificationEntity.stade ? classificationEntity.stade.id : ''}</dd>
          <dt>Unclassified</dt>
          <dd>{classificationEntity.unclassified ? classificationEntity.unclassified.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/classification" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/classification/${classificationEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default ClassificationDetail;
