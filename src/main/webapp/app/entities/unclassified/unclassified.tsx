import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { openFile, byteSize, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUnclassified } from 'app/shared/model/unclassified.model';
import { getEntities } from './unclassified.reducer';

export const Unclassified = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const unclassifiedList = useAppSelector(state => state.unclassified.entities);
  const loading = useAppSelector(state => state.unclassified.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="unclassified-heading" data-cy="UnclassifiedHeading">
        Unclassifieds
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to="/unclassified/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Unclassified
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {unclassifiedList && unclassifiedList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Code</th>
                <th>Photo</th>
                <th>Maladie</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {unclassifiedList.map((unclassified, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/unclassified/${unclassified.id}`} color="link" size="sm">
                      {unclassified.id}
                    </Button>
                  </td>
                  <td>{unclassified.code}</td>
                  <td>
                    {unclassified.photo ? (
                      <div>
                        {unclassified.photoContentType ? (
                          <a onClick={openFile(unclassified.photoContentType, unclassified.photo)}>
                            <img src={`data:${unclassified.photoContentType};base64,${unclassified.photo}`} style={{ maxHeight: '30px' }} />
                            &nbsp;
                          </a>
                        ) : null}
                        <span>
                          {unclassified.photoContentType}, {byteSize(unclassified.photo)}
                        </span>
                      </div>
                    ) : null}
                  </td>
                  <td>{unclassified.maladie ? <Link to={`/maladie/${unclassified.maladie.id}`}>{unclassified.maladie.id}</Link> : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/unclassified/${unclassified.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`/unclassified/${unclassified.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`/unclassified/${unclassified.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Unclassifieds found</div>
        )}
      </div>
    </div>
  );
};

export default Unclassified;
