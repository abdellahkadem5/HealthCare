import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Unclassified from './unclassified';
import UnclassifiedDetail from './unclassified-detail';
import UnclassifiedUpdate from './unclassified-update';
import UnclassifiedDeleteDialog from './unclassified-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UnclassifiedUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UnclassifiedUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UnclassifiedDetail} />
      <ErrorBoundaryRoute path={match.url} component={Unclassified} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={UnclassifiedDeleteDialog} />
  </>
);

export default Routes;
