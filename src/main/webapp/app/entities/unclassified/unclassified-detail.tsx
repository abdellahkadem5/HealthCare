import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './unclassified.reducer';

export const UnclassifiedDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const unclassifiedEntity = useAppSelector(state => state.unclassified.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="unclassifiedDetailsHeading">Unclassified</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{unclassifiedEntity.id}</dd>
          <dt>
            <span id="code">Code</span>
          </dt>
          <dd>{unclassifiedEntity.code}</dd>
          <dt>
            <span id="photo">Photo</span>
          </dt>
          <dd>
            {unclassifiedEntity.photo ? (
              <div>
                {unclassifiedEntity.photoContentType ? (
                  <a onClick={openFile(unclassifiedEntity.photoContentType, unclassifiedEntity.photo)}>
                    <img
                      src={`data:${unclassifiedEntity.photoContentType};base64,${unclassifiedEntity.photo}`}
                      style={{ maxHeight: '30px' }}
                    />
                  </a>
                ) : null}
                <span>
                  {unclassifiedEntity.photoContentType}, {byteSize(unclassifiedEntity.photo)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>Maladie</dt>
          <dd>{unclassifiedEntity.maladie ? unclassifiedEntity.maladie.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/unclassified" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/unclassified/${unclassifiedEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default UnclassifiedDetail;
