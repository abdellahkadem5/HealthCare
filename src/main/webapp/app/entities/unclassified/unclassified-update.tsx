import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm, ValidatedBlobField } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IMaladie } from 'app/shared/model/maladie.model';
import { getEntities as getMaladies } from 'app/entities/maladie/maladie.reducer';
import { IUnclassified } from 'app/shared/model/unclassified.model';
import { getEntity, updateEntity, createEntity, reset } from './unclassified.reducer';

export const UnclassifiedUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const maladies = useAppSelector(state => state.maladie.entities);
  const unclassifiedEntity = useAppSelector(state => state.unclassified.entity);
  const loading = useAppSelector(state => state.unclassified.loading);
  const updating = useAppSelector(state => state.unclassified.updating);
  const updateSuccess = useAppSelector(state => state.unclassified.updateSuccess);
  const handleClose = () => {
    props.history.push('/unclassified');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getMaladies({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...unclassifiedEntity,
      ...values,
      maladie: maladies.find(it => it.id.toString() === values.maladie.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...unclassifiedEntity,
          maladie: unclassifiedEntity?.maladie?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="healthCareApp.unclassified.home.createOrEditLabel" data-cy="UnclassifiedCreateUpdateHeading">
            Create or edit a Unclassified
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="unclassified-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Code" id="unclassified-code" name="code" data-cy="code" type="text" validate={{}} />
              <ValidatedBlobField label="Photo" id="unclassified-photo" name="photo" data-cy="photo" isImage accept="image/*" />
              <ValidatedField id="unclassified-maladie" name="maladie" data-cy="maladie" label="Maladie" type="select">
                <option value="" key="0" />
                {maladies
                  ? maladies.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/unclassified" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default UnclassifiedUpdate;
