import { IMaladie } from 'app/shared/model/maladie.model';
import { IPatient } from 'app/shared/model/patient.model';
import { IClassification } from 'app/shared/model/classification.model';
import { IImage } from 'app/shared/model/image.model';

export interface IStade {
  id?: number;
  code?: string | null;
  level?: string | null;
  description?: string | null;
  maladie?: IMaladie | null;
  patients?: IPatient[] | null;
  classifications?: IClassification[] | null;
  images?: IImage[] | null;
}

export const defaultValue: Readonly<IStade> = {};
