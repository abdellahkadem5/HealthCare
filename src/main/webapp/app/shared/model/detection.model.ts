import dayjs from 'dayjs';
import { IMaladie } from 'app/shared/model/maladie.model';
import { IPatient } from 'app/shared/model/patient.model';
import { IVisite } from 'app/shared/model/visite.model';

export interface IDetection {
  id?: number;
  photoContentType?: string | null;
  photo?: string | null;
  code?: string | null;
  validation?: boolean | null;
  stade?: string | null;
  date?: string | null;
  description?: string | null;
  maladie?: IMaladie | null;
  patient?: IPatient | null;
  visite?: IVisite | null;
}

export const defaultValue: Readonly<IDetection> = {
  validation: false,
};
