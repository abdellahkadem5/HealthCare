import { IStade } from 'app/shared/model/stade.model';

export interface IImage {
  id?: number;
  code?: string | null;
  photoContentType?: string | null;
  photo?: string | null;
  stade?: IStade | null;
}

export const defaultValue: Readonly<IImage> = {};
