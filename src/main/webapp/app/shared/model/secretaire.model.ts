import { IUser } from 'app/shared/model/user.model';
import { IPatient } from 'app/shared/model/patient.model';
import { IMedecin } from 'app/shared/model/medecin.model';

export interface ISecretaire {
  id?: number;
  code?: string | null;
  nom?: string | null;
  numEmp?: string | null;
  prenom?: string | null;
  admin?: boolean | null;
  photoContentType?: string | null;
  photo?: string | null;
  user?: IUser | null;
  patients?: IPatient[] | null;
  medecins?: IMedecin[] | null;
}

export const defaultValue: Readonly<ISecretaire> = {
  admin: false,
};
