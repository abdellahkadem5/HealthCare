import dayjs from 'dayjs';
import { IPatient } from 'app/shared/model/patient.model';
import { IMedecin } from 'app/shared/model/medecin.model';
import { IVisite } from 'app/shared/model/visite.model';

export interface IRendezVous {
  id?: number;
  date?: string | null;
  code?: string | null;
  status?: string | null;
  patient?: IPatient | null;
  medecin?: IMedecin | null;
  visite?: IVisite | null;
}

export const defaultValue: Readonly<IRendezVous> = {};
