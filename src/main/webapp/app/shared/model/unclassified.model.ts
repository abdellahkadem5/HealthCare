import { IMaladie } from 'app/shared/model/maladie.model';
import { IClassification } from 'app/shared/model/classification.model';

export interface IUnclassified {
  id?: number;
  code?: string | null;
  photoContentType?: string | null;
  photo?: string | null;
  maladie?: IMaladie | null;
  classifications?: IClassification[] | null;
}

export const defaultValue: Readonly<IUnclassified> = {};
