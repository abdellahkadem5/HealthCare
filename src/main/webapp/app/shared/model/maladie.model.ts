import dayjs from 'dayjs';
import { IDetection } from 'app/shared/model/detection.model';
import { IStade } from 'app/shared/model/stade.model';
import { IUnclassified } from 'app/shared/model/unclassified.model';

export interface IMaladie {
  id?: number;
  code?: string | null;
  date?: string | null;
  detection?: IDetection | null;
  stades?: IStade[] | null;
  unclassifieds?: IUnclassified[] | null;
}

export const defaultValue: Readonly<IMaladie> = {};
