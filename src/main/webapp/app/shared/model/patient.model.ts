import dayjs from 'dayjs';
import { IUser } from 'app/shared/model/user.model';
import { ISecretaire } from 'app/shared/model/secretaire.model';
import { IStade } from 'app/shared/model/stade.model';
import { IDetection } from 'app/shared/model/detection.model';
import { IRendezVous } from 'app/shared/model/rendez-vous.model';
import { Genre } from 'app/shared/model/enumerations/genre.model';

export interface IPatient {
  id?: number;
  code?: string | null;
  nom?: string | null;
  prenom?: string | null;
  dateNaissance?: string | null;
  adresse?: string | null;
  genre?: Genre | null;
  telephone?: string | null;
  poids?: number | null;
  taille?: number | null;
  photoContentType?: string | null;
  photo?: string | null;
  user?: IUser | null;
  secretaire?: ISecretaire | null;
  stade?: IStade | null;
  detections?: IDetection[] | null;
  rendezVous?: IRendezVous[] | null;
}

export const defaultValue: Readonly<IPatient> = {};
