import { IUser } from 'app/shared/model/user.model';
import { ISecretaire } from 'app/shared/model/secretaire.model';
import { IRendezVous } from 'app/shared/model/rendez-vous.model';
import { IClassification } from 'app/shared/model/classification.model';

export interface IMedecin {
  id?: number;
  code?: string | null;
  nom?: string | null;
  numEmp?: string | null;
  prenom?: string | null;
  expertLevel?: number | null;
  photoContentType?: string | null;
  photo?: string | null;
  type?: string | null;
  nbrPatients?: number | null;
  rating?: number | null;
  description?: string | null;
  user?: IUser | null;
  secretaire?: ISecretaire | null;
  rendezVous?: IRendezVous[] | null;
  classifications?: IClassification[] | null;
}

export const defaultValue: Readonly<IMedecin> = {};
