import { IMedecin } from 'app/shared/model/medecin.model';
import { IStade } from 'app/shared/model/stade.model';
import { IUnclassified } from 'app/shared/model/unclassified.model';

export interface IClassification {
  id?: number;
  code?: string | null;
  score?: boolean | null;
  medecin?: IMedecin | null;
  stade?: IStade | null;
  unclassified?: IUnclassified | null;
}

export const defaultValue: Readonly<IClassification> = {
  score: false,
};
