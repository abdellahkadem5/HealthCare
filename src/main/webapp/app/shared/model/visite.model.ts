import dayjs from 'dayjs';
import { IRendezVous } from 'app/shared/model/rendez-vous.model';
import { IDetection } from 'app/shared/model/detection.model';

export interface IVisite {
  id?: number;
  code?: string | null;
  date?: string | null;
  rendezVous?: IRendezVous | null;
  detection?: IDetection | null;
}

export const defaultValue: Readonly<IVisite> = {};
