package pat.mal.sta.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import pat.mal.sta.domain.Classification;

/**
 * Spring Data SQL repository for the Classification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClassificationRepository extends JpaRepository<Classification, Long> {}
