package pat.mal.sta.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import pat.mal.sta.domain.Unclassified;

/**
 * Spring Data SQL repository for the Unclassified entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnclassifiedRepository extends JpaRepository<Unclassified, Long> {}
