package pat.mal.sta.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import pat.mal.sta.domain.Detection;

/**
 * Spring Data SQL repository for the Detection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetectionRepository extends JpaRepository<Detection, Long> {}
