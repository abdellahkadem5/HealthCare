package pat.mal.sta.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import pat.mal.sta.domain.Visite;

/**
 * Spring Data SQL repository for the Visite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VisiteRepository extends JpaRepository<Visite, Long> {}
