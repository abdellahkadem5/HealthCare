package pat.mal.sta.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import pat.mal.sta.domain.Image;

/**
 * Spring Data SQL repository for the Image entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {}
