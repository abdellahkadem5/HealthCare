package pat.mal.sta.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import pat.mal.sta.domain.Secretaire;

/**
 * Spring Data SQL repository for the Secretaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SecretaireRepository extends JpaRepository<Secretaire, Long> {}
