/**
 * View Models used by Spring MVC REST controllers.
 */
package pat.mal.sta.web.rest.vm;
