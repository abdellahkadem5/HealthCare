package pat.mal.sta.domain.enumeration;

/**
 * The Genre enumeration.
 */
public enum Genre {
    Homme,
    Femme,
}
